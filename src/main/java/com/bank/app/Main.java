package com.bank.app;

import com.bank.service.config.BankServiceModule;
import com.bank.service.internal.Account;
import com.bank.service.internal.AccountDataStore;
import com.bank.rest.RestConfig;
import com.bank.rest.config.RestModule;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.javamoney.moneta.Money;

import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new RestModule(), new BankServiceModule(), new DataStoreModule());
        RestConfig app = injector.getInstance(RestConfig.class);
        app.start(7000);
    }


    private static class DataStoreModule extends AbstractModule {
        @Override
        protected void configure() {
            bind(AccountDataStore.class).toInstance(new AccountDataStore(getData()));
        }

        private Set<Account> getData() {
            Account inTransactionAccount = new Account("9999", Money.of(100, "USD"));
            inTransactionAccount.startTransaction();
            return Set.of(new Account("1234", Money.of(100, "USD")),
                    new Account("5678", Money.of(100, "USD")),
                    inTransactionAccount);

        }

    }


}
