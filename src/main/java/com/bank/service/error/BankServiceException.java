package com.bank.service.error;

public class BankServiceException extends RuntimeException {
    private final ErrorCode errorCode;

    public BankServiceException(ErrorCode errorCode, String msg) {
        super(msg);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
