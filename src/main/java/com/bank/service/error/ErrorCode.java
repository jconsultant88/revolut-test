package com.bank.service.error;

public enum ErrorCode {
    INSUFFICIENT_FUNDS(10001, "Insufficient funds"),
    ACCOUNT_NOT_FOUND(10002, "Account not found"),

    INCORRECT_TRANSFER_AMOUNT(10003, "Incorrect transfer amount"),
    SERVICE_BUSY(1004, "Service is busy"),
    TRANSFER_ACCOUNTS_SAME(1005, "Incorrect transfer accounts");
    private int id;
    private String message;

    ErrorCode(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
