package com.bank.service;

import com.bank.service.internal.*;
import com.google.inject.Inject;
import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.util.Optional;

public class AccountServiceImpl implements AccountService {
    private static final CurrencyUnit GBP = Monetary.getCurrency("GBP");

    private final MoneyTransferService transferService;
    private final BalanceService balanceService;
    private final AccountDataStore accountDataStore;
    private final ValidatorService validatorService;

    @Inject
    public AccountServiceImpl(AccountDataStore accountDataStore, ValidatorService validatorService, MoneyTransferService transferService, BalanceService balanceService) {
        this.transferService = transferService;
        this.balanceService = balanceService;
        this.accountDataStore = accountDataStore;
        this.validatorService = validatorService;
    }

    @Override
    public void transfer(String fromAccountId, String toAccountId, String trfAmountStr, String currency) {
        Optional<Account> fromAccount = accountDataStore.getAccount(fromAccountId);
        Optional<Account> toAccount = accountDataStore.getAccount(toAccountId);
        Money trfAmount = Money.of(Double.valueOf(trfAmountStr), currency);

        validatorService.validateMoneyTransferAccount(fromAccount, toAccount, trfAmount);

        transferService.transfer(fromAccount.get(), toAccount.get(), trfAmount);
    }

    @Override
    public Money getBalance(String accountId) {
        Optional<Account> account = accountDataStore.getAccount(accountId);
        validatorService.validateAccount(account);
        return balanceService.getBalance(account.get());
    }


}
