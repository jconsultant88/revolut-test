package com.bank.service;

import org.javamoney.moneta.Money;

public interface AccountService {
    void transfer(String fromAccountId, String toAccountId, String trfAmount, String currency);

    Money getBalance(String accountId);
}
