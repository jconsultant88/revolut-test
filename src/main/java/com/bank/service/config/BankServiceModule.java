package com.bank.service.config;

import com.bank.service.AccountService;
import com.bank.service.AccountServiceImpl;
import com.bank.service.internal.*;
import com.google.inject.AbstractModule;

public class BankServiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(AccountService.class).to(AccountServiceImpl.class);
        bind(BalanceService.class).to(BalanceServiceImpl.class);
        bind(MoneyTransferService.class).to(MoneyTransferServiceImpl.class);
        bind(ValidatorService.class).toInstance(new ValidatorService());

    }
}
