package com.bank.service.internal;

import com.bank.service.error.BankServiceException;
import com.bank.service.error.ErrorCode;
import org.javamoney.moneta.Money;

public class BalanceServiceImpl implements BalanceService {

    @Override
    public Money getBalance(Account account) {
        if (account.startTransaction()) {
            try {
                return account.getBalance();
            } finally {
                account.endTransaction();
            }
        }
        throw new BankServiceException(ErrorCode.SERVICE_BUSY, "Account locked momentarily. Try later.");
    }
}
