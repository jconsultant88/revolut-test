package com.bank.service.internal;

import com.bank.service.error.BankServiceException;
import com.bank.service.error.ErrorCode;
import org.javamoney.moneta.Money;

public class MoneyTransferServiceImpl implements MoneyTransferService {

    @Override
    public void transfer(Account fromAccount, Account toAccount, Money trfAmount) {
        if (fromAccount.startTransaction() && toAccount.startTransaction()) {
            try {
                checkValidBalance(fromAccount, trfAmount);
                fromAccount.debit(trfAmount);
                toAccount.credit(trfAmount);
                return;
            } finally {
                fromAccount.endTransaction();
                toAccount.endTransaction();
            }
        }
        throw new BankServiceException(ErrorCode.SERVICE_BUSY, "Account locked momentarily. Try later.");
    }

    private void checkValidBalance(Account fromAccount, Money trfAmount) {
        if (fromAccount.getBalance().subtract(trfAmount).isNegative()) {
            throw new BankServiceException(ErrorCode.INSUFFICIENT_FUNDS, "Insufficient funds.");
        }
    }

}
