package com.bank.service.internal;


import com.bank.service.error.BankServiceException;
import com.bank.service.error.ErrorCode;
import org.javamoney.moneta.Money;

import java.util.Optional;

public class ValidatorService {

    public void validateMoneyTransferAccount(Optional<Account> fromAccount, Optional<Account> toAccount, Money trfAmount) {
        validateAccount(fromAccount, "invalid FROM account");
        validateAccount(toAccount, "invalid TO account");
        validateTransferAccountsDifferent(fromAccount.get(), toAccount.get());
        validateAmount(trfAmount);
    }

    public void validateAccount(Optional<Account> account) {
        validateAccount(account, "invalid account");
    }

    private void validateTransferAccountsDifferent(Account fromAccount, Account toAccount) {
        if (fromAccount.getAccountId().equals(toAccount.getAccountId())) {
            throw new BankServiceException(ErrorCode.TRANSFER_ACCOUNTS_SAME, "FROM and TO Accounts same");
        }
    }

    private void validateAccount(Optional<Account> account, String message) {
        account.orElseThrow(() -> new BankServiceException(ErrorCode.ACCOUNT_NOT_FOUND, message));
    }

    private void validateAmount(Money trfAmount) {
        if (trfAmount.isNegativeOrZero()) {
            throw new BankServiceException(ErrorCode.INCORRECT_TRANSFER_AMOUNT, "Amount less than zero");
        }
    }
}
