package com.bank.service.internal;

import org.javamoney.moneta.Money;

public interface BalanceService {
    public Money getBalance(Account account);
}
