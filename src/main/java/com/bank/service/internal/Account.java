package com.bank.service.internal;

import org.javamoney.moneta.Money;

import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Account {
    private final String accountId;
    private Money balance;
    private final Lock transactionLock = new ReentrantLock();

    public Account(String accountId, Money balance) {
        this.balance = balance;
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }

    public Money getBalance() {
        return balance;
    }

    public void credit(Money amount) {
        balance = balance.add(amount);
    }

    public void debit(Money amount) {
        balance = balance.subtract(amount);
    }

    public boolean startTransaction() {
        return transactionLock.tryLock();
    }

    public void endTransaction() {
        transactionLock.unlock();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return accountId.equals(account.accountId) &&
                balance.equals(account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, balance);
    }
}
