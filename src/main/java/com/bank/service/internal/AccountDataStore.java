package com.bank.service.internal;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class AccountDataStore {
    private final CopyOnWriteArraySet<Account> dataStore ;

    public AccountDataStore(Set<Account> dataSet) {
        dataStore = new CopyOnWriteArraySet<>(dataSet);
    }

    public Optional<Account> getAccount(String accountId) {
        return dataStore.stream().filter(account -> accountId.equals(account.getAccountId())).findFirst();
    }
}
