package com.bank.service.internal;

import org.javamoney.moneta.Money;

public interface MoneyTransferService {
    void transfer(Account fromAccount, Account toAccount, Money trfAmount);
}
