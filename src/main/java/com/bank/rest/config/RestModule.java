package com.bank.rest.config;

import com.bank.rest.BankServiceRequestHandler;
import com.google.inject.AbstractModule;
import io.javalin.Javalin;

public class RestModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Javalin.class).toInstance(getApp());
        bind(BankServiceRequestHandler.class);
    }

    private Javalin getApp() {
        Javalin javalin = Javalin.create();
        return javalin;
    }
}
