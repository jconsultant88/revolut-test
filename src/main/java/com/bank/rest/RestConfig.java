package com.bank.rest;

import com.bank.service.error.BankServiceException;
import com.google.inject.Inject;
import io.javalin.Javalin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestConfig {
    Logger LOG = LoggerFactory.getLogger(RestConfig.class);
    private final BankServiceRequestHandler handler;
    private final Javalin javalin;

    @Inject
    public RestConfig(Javalin javalin, BankServiceRequestHandler handler) {
        this.handler = handler;
        this.javalin = javalin;
        init();
    }


    public void init() {
        javalin.enableCaseSensitiveUrls();
        javalin.post("/payments", handler.paymentHandler());
        javalin.get("/accounts/:accountId", handler.getBalanceHandler());

        javalin.exception(BankServiceException.class, (e, ctx) -> {
            LOG.error("Error processing request", e);
            ctx.status(400);
            ctx.json(new ErrorResponse(e));
        });
    }

    public void start(int port) {
        javalin.start(port);
    }
}
