package com.bank.rest;

import com.bank.service.AccountService;
import com.google.inject.Inject;
import io.javalin.Handler;

public class BankServiceRequestHandler {
    private final AccountService accountService;

    @Inject
    public BankServiceRequestHandler(AccountService accountService) {
        this.accountService = accountService;
    }

    public Handler paymentHandler() {
        return (ctx) -> {
            PaymentRequest paymentRequest = ctx.bodyAsClass(PaymentRequest.class);
            accountService.transfer(paymentRequest.getFromAccount(), paymentRequest.getToAccount(), paymentRequest.getTrfAmount(), paymentRequest.getCurrency());
            ctx.status(201);
        };
    }

    public Handler getBalanceHandler() {
        return (ctx) -> {
            String accountId = ctx.pathParam("accountId");
            ctx.json(new BalanceResponse(accountService.getBalance(accountId)));
        };
    }
}
