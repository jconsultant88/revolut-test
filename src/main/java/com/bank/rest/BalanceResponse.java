package com.bank.rest;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.javamoney.moneta.Money;

@JsonPropertyOrder({"currency", "balance"})
public class BalanceResponse {
    private final String balance;
    private final String currency;

    public BalanceResponse(Money amount) {
        this.balance = amount.getNumber().toString();
        this.currency = amount.getCurrency().toString();
    }

    public String getCurrency() {
        return currency;
    }

    public String getBalance() {
        return balance;
    }
}
