package com.bank.rest;

import com.bank.service.error.BankServiceException;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"errorCode", "errorMsg"})
public class ErrorResponse {
    private final BankServiceException bankServiceException;

    public ErrorResponse(BankServiceException bankServiceException) {
        this.bankServiceException = bankServiceException;
    }

    public int getErrorCode() {
        return bankServiceException.getErrorCode().getId();
    }

    public String getErrorMsg() {
        return bankServiceException.getErrorCode().getMessage();
    }

    public String getErrorDetails() {
        return bankServiceException.getMessage();
    }


}
