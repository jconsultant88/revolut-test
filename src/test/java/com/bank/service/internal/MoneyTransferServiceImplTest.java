package com.bank.service.internal;

import com.bank.service.error.BankServiceException;
import com.bank.service.error.ErrorCode;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.*;

class MoneyTransferServiceImplTest {
    private static final String FROM_ACCOUNT_ID = "1234";
    private static final String TO_ACCOUNT_ID = "5678";
    private static final String IN_TRANSACTION_ACCOUNT_ID = "9999";
    private final Account inTransactionAccount = new Account(IN_TRANSACTION_ACCOUNT_ID, Money.of(100, "USD"));
    private MoneyTransferServiceImpl moneyTransferService;
    private ExecutorService executorService;

    Set<Account> data = Set.of(new Account(FROM_ACCOUNT_ID, Money.of(100, "USD")),
            new Account(TO_ACCOUNT_ID, Money.of(100, "USD")),
            inTransactionAccount);

    @BeforeEach
    public void setUp() {
        executorService = Executors.newSingleThreadExecutor();
        inTransactionAccount.startTransaction();
        moneyTransferService = new MoneyTransferServiceImpl();
    }

    @AfterEach
    public void tearDown() {
        inTransactionAccount.endTransaction();
        executorService.shutdown();
    }


    @Test
    public void successfulMoneyTrf() {
        moneyTransferService.transfer(getAccount("1234"), getAccount("5678"), Money.of(20, "USD"));
    }

    @Test
    public void unsuccessfulMoneyTransferFromAccountUnderTransaction() throws Exception {
        Future<?> future = executorService.submit(() -> {
            BankServiceException bankServiceException = assertThrows(BankServiceException.class, () -> moneyTransferService.transfer(getAccount("9999"), getAccount("5678"), Money.of(20, "USD")));
            assertEquals(ErrorCode.SERVICE_BUSY, bankServiceException.getErrorCode());
        });
        future.get();//needed to extract the exception
    }

    @Test
    public void unsuccessfulMoneyTransferToAccountUnderTransaction() throws ExecutionException, InterruptedException {
        Future<?> future = executorService.submit(() -> {
            BankServiceException bankServiceException = assertThrows(BankServiceException.class, () -> moneyTransferService.transfer(getAccount("1234"), getAccount("9999"), Money.of(20, "USD")));
            assertEquals(ErrorCode.SERVICE_BUSY, bankServiceException.getErrorCode());
        });
        Object o = future.get();
    }

    @Test
    public void insufficientBalance() {
        BankServiceException bankServiceException = assertThrows(BankServiceException.class, () -> moneyTransferService.transfer(getAccount("1234"), getAccount("5678"), Money.of(500, "USD")));
        assertEquals(ErrorCode.INSUFFICIENT_FUNDS, bankServiceException.getErrorCode());
    }

    public Account getAccount(String accountId) {
        return data.stream().filter(account -> accountId.equals(account.getAccountId())).findFirst().get();
    }


}

