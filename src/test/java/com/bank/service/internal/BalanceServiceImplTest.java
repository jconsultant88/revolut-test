package com.bank.service.internal;

import com.bank.service.error.BankServiceException;
import com.bank.service.error.ErrorCode;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.*;

class BalanceServiceImplTest {
    private BalanceService balanceService;
    private ExecutorService executorService;
    private Account inTransactionAccount = new Account("9999", Money.of(500, "USD"));

    @BeforeEach
    public void setUp() {
        executorService = Executors.newSingleThreadExecutor();
        inTransactionAccount.startTransaction();
        balanceService = new BalanceServiceImpl();
    }

    @AfterEach
    public void tearDown() {
        inTransactionAccount.endTransaction();
        executorService.shutdown();
    }

    @Test
    public void getBalanceSuccessfully() {
        //GIVEN:
        Account account = new Account("1234", Money.of(100, "USD"));
        //WHEN:
        Money balance = balanceService.getBalance(account);
        //THEN:
        assertEquals("USD 100", balance.toString());
    }

    @Test
    public void getBalanceFailed_forCheckingIntransactionAccount() throws Exception {
        Future<?> executed = executorService.submit(() -> {
            BankServiceException bankServiceException = assertThrows(BankServiceException.class, () -> balanceService.getBalance(inTransactionAccount));
            assertEquals(ErrorCode.SERVICE_BUSY, bankServiceException.getErrorCode());
        });
        assertNull(executed.get());
    }

}