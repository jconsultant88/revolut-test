package com.bank.service.internal;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class AccountDataStoreTest {
    private static final String FROM_ACCOUNT_ID = "1234";
    Account account = new Account(FROM_ACCOUNT_ID, Money.of(100, "USD"));
    AccountDataStore accountDataStore = new AccountDataStore(Set.of(account));

    @Test
    public void accountFound() {
        //WHEN:
        Optional<Account> accountOptional = accountDataStore.getAccount(FROM_ACCOUNT_ID);

        //THEN:
        assertTrue(accountOptional.isPresent());
        assertEquals(account, accountOptional.get());
    }

    @Test
    public void accountNotFound(){
        //WHEN:
        Optional<Account> accountOptional = accountDataStore.getAccount("NonExistAccount");

        //THEN:
        assertFalse(accountOptional.isPresent());
    }
}