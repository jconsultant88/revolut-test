package com.bank.service.internal;

import com.bank.service.error.BankServiceException;
import com.bank.service.error.ErrorCode;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorServiceTest {
    private final ValidatorService validateService = new ValidatorService();

    @Test
    public void validateMoneyTransferAccount_FROM_account_notfound() {
        Optional<Account> fromAccount = Optional.ofNullable(null);
        Optional<Account> toAccount = Optional.of(new Account("1234", Money.of(100, "USD")));

        BankServiceException bankServiceException = assertThrows(BankServiceException.class, () -> validateService.validateMoneyTransferAccount(fromAccount, toAccount, Money.of(123, "USD")));
        assertEquals(ErrorCode.ACCOUNT_NOT_FOUND, bankServiceException.getErrorCode());
        assertEquals("invalid FROM account", bankServiceException.getMessage());
    }

    @Test
    public void validateMoneyTransferAccount_TO_account_notfound() {
        Optional<Account> toAccount = Optional.ofNullable(null);
        Optional<Account> fromAccount = Optional.of(new Account("1234", Money.of(100, "USD")));

        BankServiceException bankServiceException = assertThrows(BankServiceException.class, () -> validateService.validateMoneyTransferAccount(fromAccount, toAccount, Money.of(123, "USD")));
        assertEquals(ErrorCode.ACCOUNT_NOT_FOUND, bankServiceException.getErrorCode());
        assertEquals("invalid TO account", bankServiceException.getMessage());
    }

    @Test
    public void transferToSameAccountNotAllowed() {
        Optional<Account> toAccount = Optional.of(new Account("1234", Money.of(100, "USD")));
        Optional<Account> fromAccount = Optional.of(new Account("1234", Money.of(100, "USD")));

        BankServiceException bankServiceException = assertThrows(BankServiceException.class, () -> validateService.validateMoneyTransferAccount(fromAccount, toAccount, Money.of(123, "USD")));
        assertEquals(ErrorCode.TRANSFER_ACCOUNTS_SAME, bankServiceException.getErrorCode());
        assertEquals("FROM and TO Accounts same", bankServiceException.getMessage());
    }

    @Test
    public void invaildTransferAmount() {
        Optional<Account> toAccount = Optional.of(new Account("1234", Money.of(100, "USD")));
        Optional<Account> fromAccount = Optional.of(new Account("2345", Money.of(100, "USD")));
        Money trfAmount = Money.of(-1, "USD");
        BankServiceException bankServiceException = assertThrows(BankServiceException.class, () -> validateService.validateMoneyTransferAccount(fromAccount, toAccount, trfAmount));
        assertEquals(ErrorCode.INCORRECT_TRANSFER_AMOUNT, bankServiceException.getErrorCode());
        assertEquals("Amount less than zero", bankServiceException.getMessage());
    }

    @Test
    public void validTransferAmount() {
        Optional<Account> toAccount = Optional.of(new Account("1234", Money.of(100, "USD")));
        Optional<Account> fromAccount = Optional.of(new Account("2345", Money.of(100, "USD")));
        Money trfAmount = Money.of(10, "USD");
        validateService.validateMoneyTransferAccount(fromAccount, toAccount, trfAmount);
        //No error/exception thrown
    }

}