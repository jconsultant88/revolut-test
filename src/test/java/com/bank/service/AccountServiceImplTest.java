package com.bank.service;

import com.bank.service.error.BankServiceException;
import com.bank.service.error.ErrorCode;
import com.bank.service.internal.*;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class AccountServiceImplTest {
    @Mock
    MoneyTransferService transferService;
    @Mock
    BalanceService balanceService;
    @Mock
    AccountDataStore accountDataStore;
    @Mock
    ValidatorService validatorService;
    @InjectMocks
    AccountServiceImpl accountService;


    @Test
    public void transfer() {
        //GIVEN:
        Account accountFrom = mock(Account.class);
        Account accountTo = mock(Account.class);
        Optional<Account> optionalFromAccount = Optional.of(accountFrom);
        Optional<Account> optionalToAccount = Optional.of(accountTo);
        Mockito.lenient().when(accountDataStore.getAccount("fromAccount")).thenReturn(optionalFromAccount);
        Mockito.lenient().when(accountDataStore.getAccount("toAccount")).thenReturn(optionalToAccount);

        //WHEN:
        accountService.transfer("fromAccount", "toAccount", "100", "USD");

        //THEN:
        //verify call to validator service
        ArgumentCaptor<Optional<Account>> fromAccountArgumentCaptor = ArgumentCaptor.forClass(Optional.class);
        ArgumentCaptor<Optional<Account>> toAccountArgumentCaptor = ArgumentCaptor.forClass(Optional.class);
        ArgumentCaptor<Money> trfMoney = ArgumentCaptor.forClass(Money.class);
        verify(validatorService, times(1)).validateMoneyTransferAccount(fromAccountArgumentCaptor.capture(), toAccountArgumentCaptor.capture(), trfMoney.capture());
        assertEquals(optionalFromAccount, fromAccountArgumentCaptor.getValue());
        assertEquals(optionalToAccount, toAccountArgumentCaptor.getValue());
        assertEquals(Money.of(Double.valueOf("100"), "USD"), trfMoney.getValue());

        //verify call to transfer service
        ArgumentCaptor<Account> fromAccountCaptor = ArgumentCaptor.forClass(Account.class);
        ArgumentCaptor<Account> toAccountCaptor = ArgumentCaptor.forClass(Account.class);
        ArgumentCaptor<Money> trfMoneyCaptor = ArgumentCaptor.forClass(Money.class);
        verify(transferService, times(1)).transfer(fromAccountCaptor.capture(), toAccountCaptor.capture(), trfMoneyCaptor.capture());
        assertEquals(accountFrom, fromAccountCaptor.getValue());
        assertEquals(accountTo, toAccountCaptor.getValue());
        assertEquals(Money.of(Double.valueOf("100"), "USD"), trfMoneyCaptor.getValue());
    }

    @Test
    public void getBalance() {
        //GIVEN:
        Account account = mock(Account.class);
        Optional<Account> optionalAccount = Optional.of(account);
        Mockito.lenient().when(accountDataStore.getAccount("accountId")).thenReturn(optionalAccount);
        Mockito.lenient().when(balanceService.getBalance(account)).thenReturn(Money.of(500, "USD"));

        //WHEN:
        Money money = accountService.getBalance("accountId");

        //THEN:

        assertEquals(Money.of(500, "USD"), money);

        //verify call to validator service
        ArgumentCaptor<Optional<Account>> accountArgumentCaptor = ArgumentCaptor.forClass(Optional.class);
        verify(validatorService, times(1)).validateAccount(accountArgumentCaptor.capture());
        assertEquals(optionalAccount, accountArgumentCaptor.getValue());

        //verify call to getBalance
        ArgumentCaptor<Account> accountCaptor = ArgumentCaptor.forClass(Account.class);
        verify(balanceService, times(1)).getBalance(accountCaptor.capture());
        assertEquals(account, accountCaptor.getValue());

    }
}